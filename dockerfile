FROM rocker/shiny-verse:4

LABEL "AUTHOR" "Migale help-migale@inrae.fr"
LABEL "VERSION" "2023-02-16 - 02"

## Dependances
RUN R -q -e "remotes::install_github('igraph/rigraph')"
RUN R -q -e "BiocManager::install('phyloseq')"
RUN R -q -e "remotes::install_github('mahendra-mariadassou/phyloseq-extended@dev', INSTALL_opts=c('--no-help', '--no-html'))"
RUN R -q -e "remotes::install_github('mahendra-mariadassou/affiliationExplorer', INSTALL_opts=c('--no-help', '--no-html'))"

## Appli
EXPOSE 3838
CMD  ["R", "-e", "options('shiny.port'=3838, shiny.host='0.0.0.0') ; affiliationExplorer::run_app(launch.browser=FALSE, max_size=100*1024^2)"]
